package fr.centralesupelec.restapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController


@SpringBootApplication
@RestController
class DemoApplication {
    @GetMapping("/helloworld")
    fun hello(): String {
        return "Hello World!"
    }
}