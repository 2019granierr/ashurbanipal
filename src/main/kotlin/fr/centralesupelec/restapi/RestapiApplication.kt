package fr.centralesupelec.restapi

import fr.centralesupelec.restapi.controller.Controller
import fr.centralesupelec.restapi.db.Connect
import org.springframework.boot.autoconfigure.SpringBootApplication
import java.time.LocalDate

@SpringBootApplication
class RestapiApplication

fun main(args: Array<String>) {
	Connect.createTables()
	Controller()



	// launches "web" server
	//runApplication<RestapiApplication>(*args)
}
