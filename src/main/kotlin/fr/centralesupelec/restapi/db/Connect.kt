package fr.centralesupelec.restapi.db
import java.sql.*
import java.util.*

object Connect {

    fun connect() {
        //Class.forName("org.sqlite.JDBC");
        var conn: Connection? = null
        try {
            // db parameters
            val url = "jdbc:sqlite:C:/sqlite/db/chinook.db"
            // create a connection to the database
            conn = DriverManager.getConnection(url)
            println("Connection to SQLite has been established.")
        } catch (e: SQLException) {
            println(e.message)
        } finally {
            try {
                conn?.close()
            } catch (ex: SQLException) {
                println(ex.message)
            }
        }
    }

    fun executeQuery(sql: String, operation: String): Pair<MutableList<Map<String, Any>>?, Int> {
        // Executes the query and returns the result set, if there is one; otherwise returns None.
        // Also returns a second parameter that is an exit code.
        /*  0: Success
            1: Failure
         */

        // SQLite connection string
        val url = "jdbc:sqlite:D://Users/rapha/Downloads/restapi/restapi/src/main/kotlin/fr/centralesupelec/restapi/db/library.db"
        try {
            DriverManager.getConnection(url).use { conn ->
                conn.createStatement().use { stmt ->
                    if (stmt.execute(sql)) {
                        // This means there is a result, eg something was fetched, as opposed to, say, inserting something.
                        println("Successfully $operation")

                        val resultList: MutableList<Map<String, Any>> = ArrayList()
                        var row: MutableMap<String, Any>? = null
                        val rs = stmt.resultSet

                        val metaData: ResultSetMetaData = rs.metaData
                        val columnCount: Int = metaData.columnCount

                        while (rs.next()) {
                            row = HashMap()
                            for (i in 1..columnCount) {
                                if (rs.getObject(i) != null) {
                                    row[metaData.getColumnName(i)] = rs.getObject(i)
                                }
                            }
                            resultList.add(row)
                        }

                        return Pair(resultList, 0)
                    } else {
                        println("Successfully $operation")
                        return Pair(null, 0)
                    }
                }
            }
        } catch (e: SQLException) {
            println("sql query: $sql")
            println(e.message)
            return Pair(null, 1)
        }
    }

    fun createTables() {

        // SQL statement for creating a new table
        val sql_USERS = """CREATE TABLE IF NOT EXISTS users (
            user_id integer NOT NULL,
            lastname text NOT NULL,
            firstname text NOT NULL,
            password text NOT NULL,
            userrole text CHECK(userrole IN ('ADMIN', 'MEMBER')) NOT NULL DEFAULT 'MEMBER',
            is_delete integer DEFAULT 0,
            is_banned integer DEFAULT 0,
            UNIQUE(lastname, firstname),
            PRIMARY KEY(user_id)
            );"""
        val sql_BOOKS = """CREATE TABLE IF NOT EXISTS books (
            book_id integer NOT NULL,
            title text NOT NULL,
            author text NOT NULL,
            genre text CHECK(genre IN ('Policier', 'Fantastique', 'Poesie', 'Autres')) NOT NULL DEFAULT 'Autres',
            is_delete integer DEFAULT 0,
            PRIMARY KEY(book_id)
            );"""
        val sql_LOANS = """CREATE TABLE IF NOT EXISTS loans (
            loanbook integer NOT NULL,
            loanuser integer NOT NULL,
            loandate text NOT NULL,
            loanreturn text NOT NULL,
            loanreturned text,
            FOREIGN KEY(loanbook) REFERENCES books(book_id),
            FOREIGN KEY(loanuser) REFERENCES users(user_id)
            );"""

        // create tables
        executeQuery(sql_USERS, "built table USERS.")
        executeQuery(sql_BOOKS, "built table BOOKS.")
        executeQuery(sql_LOANS, "built table LOANS.")

    }

}