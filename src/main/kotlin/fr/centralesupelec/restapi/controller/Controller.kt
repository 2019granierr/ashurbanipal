package fr.centralesupelec.restapi.controller

import fr.centralesupelec.restapi.GUI.Homepage
import fr.centralesupelec.restapi.db.Connect
import fr.centralesupelec.restapi.model.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import java.time.LocalDate
import java.time.format.DateTimeFormatter




class Controller {
    val GUI = Homepage(this)
    val formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd")

    @Autowired
    var jdbcTemplate = JdbcTemplate()

    fun loginUserC(firstName: String, lastName: String, password: String): Pair<Int, Role> {
        val user = User(firstName, lastName, password, Role.UNKNOWN)
        val sql = """SELECT userrole FROM users WHERE firstname = "${user.firstName}" AND lastname="${user.lastName}";"""
        val (rs, exitCode) = Connect.executeQuery(sql, "tried to login ${user.firstName} ${user.lastName}")
        if (exitCode == 0) {
            try {
                val role: String = (rs?.get(0)?.get("userrole") as String)
                return Pair(exitCode, Role.valueOf(role))
            } catch (e: IndexOutOfBoundsException) {
                return Pair(1, Role.UNKNOWN)
            }
        } else {
            return Pair(1, Role.UNKNOWN)
        }
    }

    fun registerUserC(firstName: String, lastName: String, password: String): Int {
        val user = User(firstName, lastName, password, Role.MEMBER)
        val sql = """INSERT INTO users(lastname, firstname, password, userrole)
                VALUES("${user.lastName}", "${user.firstName}", "${user.password}", "${user.role}");"""
        val (_, exitCode) = Connect.executeQuery(sql, "tried to register ${user.firstName} ${user.lastName}.")
        return exitCode
    }

    fun addBookC(title: String, author: String, genre: Genre, numberOfBooksToAdd: Int): Int {
        val book = Book(title, author, genre)
        val sql = """INSERT INTO books(title, author, genre)
                VALUES("${book.title}", "${book.author}", "${book.genre}");"""
        var exitCodeCum = 0
        for (i in (0 until numberOfBooksToAdd)) {
            val (_, exitCode) = Connect.executeQuery(sql, "tried to add ${book.title} by ${book.author}.")
            exitCodeCum += exitCode
        }
        if (exitCodeCum != 0) {
            return 1
        }
        return 0
    }

    fun searchBookC(title: String, author: String, genre: Genre): Pair<Int, MutableList<Book>?> {

        val words = title.split("\\s+".toRegex()).map { word ->
            word.replace("""^[,\.]|[,\.]$""".toRegex(), "")
        }

        var sqlWords = words.joinToString(separator = """%" OR "%""", prefix = """ "%""", postfix = """%" """)
        if (words[0] == "") {
            println("words is empty!")
            sqlWords = """ "%" """
        }

        val sqlAuthor: String
        if (author == "") {
            sqlAuthor = """"%""""
        } else {
            sqlAuthor = """"%$author%""""
        }

        val sql: String
        if (genre == Genre.Quelconque) {
            sql = """
                SELECT title, author, genre, book_id FROM books
                WHERE (title LIKE $sqlWords)
                AND (author LIKE $sqlAuthor)
                AND is_delete = 0
            """.trimIndent()
        } else {
            sql = """
                SELECT title, author, genre, book_id FROM books
                WHERE (title LIKE $sqlWords)
                AND (author LIKE $sqlAuthor)
                AND genre = "$genre"
                AND is_delete = 0
            """.trimIndent()
        }

        //println(sql)
        val (rs, exitCode) = Connect.executeQuery(sql, "tried to search for title: '$title' by author: '$author'.")
        if (exitCode == 0) {
            try {

                if (rs != null) {
                    val bookList: MutableList<Book> = mutableListOf()
                    for (row in rs) {
                        val book = Book(row["title"] as String, row["author"] as String, Genre.valueOf(row["genre"] as String), row["book_id"] as Int)
                        bookList.add(book)
                    }
                    return Pair(0, bookList)
                }

                return Pair(0, null)
            } catch (e: IndexOutOfBoundsException) {
                return Pair(1, null)
            }
        } else {
            return Pair(1, null)
        }
    }



    fun getLoanStatus(book: Book): Pair< Int, Loan? > {
        val sql = """
            SELECT firstname, lastname, loandate, loanreturn, loanreturned , title, author
            FROM (loans INNER JOIN books ON loans.loanbook = books.book_id) INNER JOIN users ON loans.loanuser = users.user_id
            WHERE books.book_id = "${book.Id}"
            ORDER BY date(loandate) DESC Limit 1
        """.trimIndent()

        val (rs, exitCode) = Connect.executeQuery(sql, "tried to search for loan of book: '${book.title}' by author: '${book.author}'.")
        if (exitCode == 0) {
            try {
                if (rs != null && rs.toString() != "[]") {
                    val loan = fetchLoanResultInfo(rs, 0)[0]
                    return Pair(0, loan)
                }

                return Pair(0, null)
            } catch (e: IndexOutOfBoundsException) {
                return Pair(1, null)
            }
        } else {
            return Pair(1, null)
        }

    }

    fun editBookGenre(book: Book, genre: Genre): Int {
        val sql = """
            UPDATE books
            SET genre = '$genre'
            WHERE title = '${book.title}' AND author = '${book.author}';
        """.trimIndent()

        val (_, exitCode) = Connect.executeQuery(sql, "tried to update ${book.title}'s genre from ${book.genre} to $genre'.")
        return exitCode
    }

    fun bookReturned(bookId: Int): Int {
        val currentDate = LocalDate.now().format(formatter)

        val sql = """
            UPDATE loans
            SET loanreturned = "$currentDate"
            WHERE loanbook = $bookId AND (loanreturned IS NULL OR loanreturned="")
        """.trimIndent()


        val (_, exitCode) = Connect.executeQuery(sql, "tried to update book n°$bookId as returned to the library.")
        return exitCode
    }

    fun prolongLoan(bookId: Int, date: LocalDate): Int {
        val dueDate = date.format(formatter)

        val sql = """
            UPDATE loans
            SET loanreturn = "$dueDate"
            WHERE loanbook = $bookId AND (loanreturned IS NULL OR loanreturned="")
        """.trimIndent()

        val (_, exitCode) = Connect.executeQuery(sql, "tried to update loan duedate of book n°$bookId.")
        return exitCode
    }


    fun getBookLoanHistory(book: Book): Pair<Int, MutableList<Loan>?> {
        val sql = """SELECT firstname, lastname, loandate, loanreturn, loanreturned, title, author 
            FROM (loans INNER JOIN books ON loans.loanbook = books.book_id) INNER JOIN users ON loans.loanuser = users.user_id
            WHERE book_id = "${book.Id}"
        """.trimIndent()
        val (rs, exitCode) = Connect.executeQuery(sql, "tried to search for loan history of book: '${book.title}' by author: '${book.author}'.")
        if (exitCode == 0) {
            try {
                if (rs != null && rs.toString() != "[]") {
                        val loanList = fetchLoanResultInfo(rs, rs.size - 1)
                        return Pair(0, loanList)
                    }
                return Pair(0, null)
            } catch (e: IndexOutOfBoundsException) {
                return Pair(1, null)
            }
        } else {
            return Pair(1, null)
        }

    }

    fun getUserLoanHistory(user: User): Pair<Int, MutableList<Loan>?> {
        val sql = """SELECT firstname, lastname, loandate, loanreturn, loanreturned, title, author
            FROM (loans INNER JOIN books ON loans.loanbook = books.book_id) INNER JOIN users ON loans.loanuser = users.user_id
            WHERE users.user_id = "${user.Id}"
        """.trimIndent()

        println("User id: ${user.Id}")

        val (rs, exitCode) = Connect.executeQuery(sql, "tried to search for loan history of user:" +
                " '${user.firstName}' '${user.lastName}'.")
        if (exitCode == 0) {
            try {
                if (rs != null && rs.toString() != "[]") {
                    val loanList = fetchLoanResultInfo(rs, rs.size - 1)
                    return Pair(0, loanList)
                }
                return Pair(0, null)
            } catch (e: IndexOutOfBoundsException) {
                return Pair(1, null)
            }
        } else {
            return Pair(1, null)
        }

    }

    fun deleteBook(book: Book): Int {

        val sql = """
            UPDATE books
            SET is_delete = 1
            WHERE book_id = ${book.Id}
        """.trimIndent()

        val (_, exitCode) = Connect.executeQuery(sql, "marked book n°${book.Id} as deleted.")
        return exitCode
    }

    fun isDeletedBook(book_id: Int): Boolean {
        val sql = """
            SELECT is_delete FROM books
            WHERE book_id = $book_id
        """.trimIndent()

        val (rs, exitCode) = Connect.executeQuery(sql, "checked deletion of book n°$book_id.")
        try {
            return rs?.get(0)?.get("is_delete") != 0
        } catch  (e: Exception){
            println("Exception in method isDeletedBook on book $book_id, assuming book is deleted.")
            return true
        }
    }

    fun isDeletedUser(user: User): Boolean {
        val sql = """
            SELECT is_delete FROM users
            WHERE firstname = "${user.firstName}" and lastname = "${user.lastName}"
        """.trimIndent()

        val (rs, exitCode) = Connect.executeQuery(sql, "checked deletion of ${user.firstName} ${user.lastName}.")
        try {
            return rs?.get(0)?.get("is_delete") != 0
        } catch  (e: Exception){
            println("Exception in method isDeletedBook on ${user.firstName} ${user.lastName}, assuming user is deleted.")
            return true
        }
    }

    fun lend(book: Book, user: User, duration: String): Int {
        // duration is in "1 semaine", "2 semaines", "3 semaines", "4 semaines"

        val dateNowString = LocalDate.now().format(formatter)
        val dueDate: LocalDate? = when (duration) {
            "1 semaine" -> {
                LocalDate.now().plusWeeks(1)
            } "2 semaines" -> {
                LocalDate.now().plusWeeks(2)
            } "3 semaines" -> {
                LocalDate.now().plusWeeks(3)
            } "un mois" -> {
                LocalDate.now().plusWeeks(4)
            }
            else -> null
        }

        val dueDateString = dueDate?.format(formatter) ?: return 1

        val sql = """
            INSERT INTO loans (loanbook, loanuser, loandate, loanreturn)
            VALUES(${book.Id}, ${user.Id}, "$dateNowString", "$dueDateString"); 
        """.trimIndent()
        print(sql)

        val (_, exitCode) = Connect.executeQuery(sql, "tried to lend to ${user.firstName} ${user.lastName}.")
        return exitCode
    }

    fun getUserId(user: User): Pair<Int?, Int> {

        val sql = """
            SELECT user_id FROM users
            WHERE firstname = "${user.firstName}" AND lastname = "${user.lastName}"
        """.trimIndent()

        val (rs, exitCode) = Connect.executeQuery(sql, "tried to retrieve user id of ${user.firstName} ${user.lastName}.")

        if (rs != null) {
            if (rs[0]["user_id"] != null && rs[0]["user_id"] != "") {
                return Pair(rs[0]["user_id"] as Int, 0)
            }
        }
        return Pair(null, 1)
    }

    fun searchUser(user: User): Pair<User, Int> {
        val sql = """
            SELECT * FROM users
            WHERE firstname = "${user.firstName}" AND lastname = "${user.lastName}"
        """.trimIndent()

        val (rs, exitCode) = Connect.executeQuery(sql, "tried to retrieve ${user.firstName} ${user.lastName}'s info.")

        if (rs != null) {
            if (rs[0]["user_id"] != null && rs[0]["user_id"] != "") {
                val user = User(rs[0]["firstname"] as String,
                        rs[0]["lastname"] as String,
                        null,
                        Role.valueOf(rs[0]["userrole"] as String),
                rs[0]["user_id"] as Int,
                rs[0]["is_delete"] as Int,
                rs[0]["is_banned"] as Int)
                return Pair(user, 0)
            }
        }
        return Pair(User("","",null,null), 1)

    }

    fun searchAllUsers(): Pair<MutableList<User>?, Int> {
        // On vérifie que l'utilisateur a fait au moins un emprunt, sinon c'est peut-être
        // juste un compte créé pour rigoler de quelqu'un qui n'a jamais mis les pieds
        // à la bibliothèque
        val sql = """
            SELECT * FROM users
            INNER JOIN loans ON loans.loanuser = users.user_id
            WHERE users.is_delete = 0
        """.trimIndent()

        val (rs, exitCode) = Connect.executeQuery(sql, "tried to retrieve all users' info.")

        if (exitCode == 0) {
            if (rs != null) {

                val userList: MutableList<User> = mutableListOf()
                for (row in rs) {
                    if (row != null) {
                        val user = User(row["firstname"] as String, row["lastname"] as String, null,
                                Role.valueOf(row["userrole"] as String), row["user_id"] as Int,0, row["is_banned"] as Int)
                        userList.add(user)
                    }
                }
                return Pair(userList, 0)

            }
            return Pair(null, 1)
        } else {
            return Pair(null, 1)
        }
    }

    fun ban(user: User): Int {
        val sql = """
            UPDATE users
            SET is_banned = 1
            WHERE user_id = ${user.Id}
        """.trimIndent()

        val (_, exitCode) = Connect.executeQuery(sql, "banned user n°${user.Id}")
        return exitCode
    }
    fun unban(user: User): Int {
        val sql = """
            UPDATE users
            SET is_banned = 0
            WHERE user_id = ${user.Id}
        """.trimIndent()

        val (_, exitCode) = Connect.executeQuery(sql, "unbanned user n°${user.Id}")
        return exitCode
    }

    fun deleteUser(user: User): Int {

        val sql = """
            UPDATE users
            SET is_delete = 1
            WHERE user_id = ${user.Id}
        """.trimIndent()

        val (_, exitCode) = Connect.executeQuery(sql, "marked user n°${user.Id} as deleted.")
        return exitCode
    }

    fun getCurrentLoans(): Pair<Int, MutableList<Loan>?> {
        val sql = """
            SELECT *
            FROM (loans INNER JOIN books ON loans.loanbook = books.book_id) INNER JOIN users ON loans.loanuser = users.user_id
            WHERE loanreturned IS NULL
        """.trimIndent()

        val (rs, exitCode) = Connect.executeQuery(sql, "tried to search all current loans.")
        print(rs)
        if (exitCode == 0) {
            try {
                if (rs != null && rs.toString() != "[]") {
                    val loanList = fetchLoanResultInfo(rs, rs.size - 1)
                    return Pair(0, loanList)
                }
                return Pair(0, null)
            } catch (e: IndexOutOfBoundsException) {
                return Pair(1, null)
            }
        } else {
            return Pair(1, null)
        }

    }

}


private fun fetchLoanResultInfo(rs: MutableList<Map<String, Any>>, i: Int): MutableList<Loan> {

    val loanList: MutableList<Loan> = mutableListOf()

    for (i in (0..i)) {
        val loanDateString: String = rs[i]["loandate"] as String
        val returnDateString: String = rs[i]["loanreturn"] as String
        val returnedDateString: String? = rs[i]["loanreturned"] as String?
        val formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd")
        val loanDate = LocalDate.parse(loanDateString, formatter)
        val returnDate = LocalDate.parse(returnDateString, formatter)
        val returnedDate: LocalDate? =
                if (returnedDateString != null && returnedDateString != ""){
                    LocalDate.parse(returnedDateString, formatter)
                } else {
                    null
                }

        val user = User(rs[i]["firstname"] as String, rs[i]["lastname"] as String, null, null)
        val book = Book(rs[i]["title"] as String, rs[i]["author"] as String, Genre.Quelconque)
        val loan = Loan(book, user, loanDate, returnDate, returnedDate)
        loanList.add(loan)
    }

    return loanList
}