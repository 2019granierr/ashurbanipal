package fr.centralesupelec.restapi.GUI.admin;

import fr.centralesupelec.restapi.GUI.member.MemberBookView
import fr.centralesupelec.restapi.GUI.SearchBookView.Companion.search
import fr.centralesupelec.restapi.controller.Controller
import fr.centralesupelec.restapi.model.Book
import fr.centralesupelec.restapi.model.Genre
import fr.centralesupelec.restapi.model.Role
import java.awt.Dimension
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import javax.swing.*
import javax.swing.table.DefaultTableModel


class SearchBookResultsView(private var controller: Controller, private var bookList: MutableList<Book>,
private val queryTitle: String, private val queryAuthor: String, private val queryGenre: Genre,
private val userrole: Role): JFrame() {
    private val panel = JPanel()

    init {
        setupMainFrame()
        addViews()
        this.contentPane.add(panel)
    }

    private fun setupMainFrame() {
        // Define frame, which is the main window on which every component relies
        this.title = "Bibliothèque Ashurbanipal"
        this.defaultCloseOperation = JFrame.DISPOSE_ON_CLOSE
        this.size = Dimension(600, 300)
        this.isVisible = true
    }


    private fun addViews() {
        val refreshButton = JButton("Rafraîchir")
        refreshButton.addActionListener {
            search(panel, controller, queryTitle, queryAuthor, queryGenre, userrole)
            dispose()
        }
        panel.add(refreshButton)

        val columnNames = arrayOf("ID", "Titre", "Auteur", "Genre", "Disponibilité")
        val data = bookListToArray()
        val table = JTable(data, columnNames)

        // On double click of table cell
        table.addMouseListener(object : MouseAdapter() {
            override fun mouseClicked(me: MouseEvent) {
                if (me.clickCount == 2) {     // to detect double click events

                    val target = me.source as JTable
                    val row = target.selectedRow // select a row

                    val bookId = (table.model.getValueAt(row, 0) as String).toInt()

                    if (controller.isDeletedBook(bookId)) {
                        JOptionPane.showMessageDialog(panel,
                                "Le livre a été supprimé.",
                                "Erreur",
                                JOptionPane.ERROR_MESSAGE);
                    } else {
                        val selectedBookTitle = table.model.getValueAt(row, 1).toString()
                        val selectedBookAuthor = table.model.getValueAt(row, 2).toString()
                        val selectedBookGenre = Genre.valueOf(table.model.getValueAt(row, 3) as String)
                        // println("$selectedBookTitle $selectedBookAuthor $selectedBookGenre")
                        val selectedBook = Book(selectedBookTitle, selectedBookAuthor, selectedBookGenre, bookId)

                        if (userrole == Role.ADMIN){
                            AdminBookView(controller, selectedBook)
                        } else {
                            MemberBookView(controller, selectedBook)
                        }

                    }
                }
            }
        })

        //following block makes cells not editable
        val tableModel: DefaultTableModel = object : DefaultTableModel(data, columnNames) {
            override fun isCellEditable(row: Int, column: Int): Boolean {
                return false
            }
        }
        table.model = tableModel

        val scrollPane = JScrollPane(table)
        panel.add(scrollPane)
    }



    private fun bookListToArray(): Array<Array<String>> {
        val mutableBookList: MutableList<Array<String>> = mutableListOf()
        for (book in bookList) {
            val (callbackCode, loan) = this.controller.getLoanStatus(book)
            println("Loan returned date: " + loan?.returnedDate)

            var availabilityString = String()
            when (callbackCode) {
                0 -> {
                    if (loan != null && loan.returnedDate == null) {
                        availabilityString = loan.dueDate.toString()
                    } else {
                        availabilityString = "Disponible"
                    }
                }
                1 -> {
                    availabilityString = "Non trouvé"
                }
            }
            mutableBookList.add( arrayOf(book.Id.toString(), book.title, book.author, book.genre.name, availabilityString))
        }

        return mutableBookList.toTypedArray()
    }



}
