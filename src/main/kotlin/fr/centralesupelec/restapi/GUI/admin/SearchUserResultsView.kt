package fr.centralesupelec.restapi.GUI.admin

import fr.centralesupelec.restapi.controller.Controller
import fr.centralesupelec.restapi.model.User
import java.awt.BorderLayout
import java.awt.Dimension
import java.awt.FlowLayout
import javax.swing.*


class SearchUserResultsView(private var controller: Controller, private val user: User): JFrame() {
    private val panel = JPanel()
    private val mainInfoLabel = JLabel()
    private val switchBanStatus = JButton()
    private val loanHistoryButton = JButton("Historique")
    private val deleteButton = JButton("Supprimer")


    init {
        setupMainFrame()
        addViews()
        this.contentPane.add(panel)
    }

    private fun setupMainFrame() {
        // Define frame, which is the main window on which every component relies
        this.title = "Bibliothèque Ashurbanipal"
        this.defaultCloseOperation = JFrame.DISPOSE_ON_CLOSE
        this.size = Dimension(500, 500)
        this.isVisible = true
        panel.layout = FlowLayout()
    }

    private fun addViews() {
        if (user.isDelete == 1) {
            JOptionPane.showMessageDialog(panel,
                    "L'utilisateur N°${user.Id} a été supprimé d'Ashurbanipal.",
                    "Erreur",
                    JOptionPane.ERROR_MESSAGE)
            println("Trying to view deleted user")
            this.dispose()
        }

        val roleString = if (user.isBanned == 0) {
            "<p>Role : ${user.role}</p>"
        } else {
            "<p>Role : ${user.role} <span style=\"color:red;\"><b>(à régulariser)</b></span></p>"
        }

        val loanHistoryString = getCurrentLoansString(controller, user)

        mainInfoLabel.text = """
            <html><body> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <h3>${user.firstName} ${user.lastName}</h3>
                $roleString
                <h4>Emprunts en cours</h4>
                $loanHistoryString
            </html></body>
        """.trimIndent()

        val scroller = JScrollPane(mainInfoLabel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED)
        scroller.size = Dimension(480,250)
        panel.add(scroller)

        if (user.isBanned == 0) {
            switchBanStatus.text = "Lister rouge"
            switchBanStatus.addActionListener {
                controller.ban(user)
                JOptionPane.showMessageDialog(panel,
                        "${user.firstName} ${user.lastName} est placé sur liste rouge",
                        "Succès",
                        JOptionPane.PLAIN_MESSAGE)
                val newRoleUser = User(user.firstName, user.lastName, null, user.role, user.Id,
                        user.isDelete, 1)
                SearchUserResultsView(controller, newRoleUser)
                this.dispose()

            }
        } else {
            switchBanStatus.text = "Délister rouge"
            switchBanStatus.addActionListener {
                controller.unban(user)
                JOptionPane.showMessageDialog(panel,
                        "${user.firstName} ${user.lastName} est retiré de la liste rouge: qu'il lise !",
                        "Succès",
                        JOptionPane.PLAIN_MESSAGE)
                val newRoleUser = User(user.firstName, user.lastName, null, user.role, user.Id,
                        user.isDelete, 0)
                SearchUserResultsView(controller, newRoleUser)
                this.dispose()
            }
        }

        panel.add(switchBanStatus)

        loanHistoryButton.addActionListener {
            LoanHistoryDialog(controller, user)
        }
        panel.add(loanHistoryButton)

        deleteButton.addActionListener {
            controller.deleteUser(user)
            JOptionPane.showMessageDialog(panel,
                    "${user.firstName} ${user.lastName} a été supprimé de la liste",
                    "Succès",
                    JOptionPane.PLAIN_MESSAGE)
            this.dispose()
        }
        panel.add(deleteButton)
    }
}

fun getCurrentLoansString(controller: Controller, user: User): String {
    var loanHistoryString = ""
    val (callbackCode, loanHistory) = controller.getUserLoanHistory(user)
    if (callbackCode == 0) {
        if (loanHistory != null) {
            for (loan in loanHistory) {
                if (loan.returnedDate == null) {
                    loanHistoryString += "<p><i>${loan.book.title}</i> de ${loan.book.author}: " +
                            "emprunt du ${loan.madeDate} au ${loan.dueDate}</p>"
                }
            }
            return loanHistoryString
        } else {
            println("Aucun emprunt en cours n'a été trouvé.")
        }
    } else {
        println("Erreur lors de la recherche de l'historique d'emprunt.")
    }
    return ""
}

public class LoanHistoryDialog(controller: Controller, user: User) : JDialog() {
    private val scrollPane = JScrollPane()

    init {
        setBounds(100, 100, 600, 300)
        defaultCloseOperation = DISPOSE_ON_CLOSE
        isVisible = true
        contentPane.layout = BorderLayout(0, 0)
        contentPane.add(scrollPane)

        val (callbackCode, loanList) = controller.getUserLoanHistory(user)
        var labelString = "<html><body><h3>   Historique des emprunts de ${user.firstName} ${user.lastName}</h3><br><br>"
        if (callbackCode == 0) {
            if (loanList != null) {
                for (loan in loanList) {
                    labelString += "<p>-   <i>${loan.book.title}</i> de ${loan.book.author} : " +
                            "emprunt du ${loan.madeDate} au ${loan.dueDate}"
                    labelString += if (loan.returnedDate != null) {
                        if (loan.returnedDate > loan.dueDate) {
                            ", rendu le <span style=\"color:red;\">${loan.returnedDate}</span>"

                        } else {
                            ", rendu le ${loan.returnedDate}</p>"
                        }
                    } else {
                        ".</p>"
                    }
                }
            }
        } else {
            labelString += "<p>Erreur de chargement de l'historique des emprunts.</p>"
        }
        labelString += "</body></html>"
        val lblContent = JLabel(labelString)
        scrollPane.setViewportView(lblContent)
    }

}