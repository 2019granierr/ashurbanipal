package fr.centralesupelec.restapi.GUI.admin

import fr.centralesupelec.restapi.controller.Controller
import fr.centralesupelec.restapi.model.Book
import fr.centralesupelec.restapi.model.User
import java.awt.Dimension
import java.awt.GridLayout
import javax.swing.*

class LendBookView(val controller: Controller, val book: Book): JFrame() {
    private val panel = JPanel()

    private val firstName = JLabel("Prénom", SwingConstants.CENTER)
    private val inputFirstName = JTextArea()
    private val lastName = JLabel("Nom", SwingConstants.CENTER)
    private val inputLastName = JTextArea()
    private val durationLabel = JLabel("Durée", SwingConstants.CENTER)
    private val durationCbbList = arrayOf("1 semaine", "2 semaines", "3 semaines", "un mois")
    private val durationCbb = JComboBox(durationCbbList)
    private val lendButton = JButton("Prêter")



    init {
        setupMainFrame()
        addViews()
        this.contentPane.add(panel)
    }

    private fun setupMainFrame() {
        // Define frame, which is the main window on which every component relies
        this.title = "Bibliothèque Ashurbanipal"
        defaultCloseOperation = DISPOSE_ON_CLOSE
        this.size = Dimension(300, 150)
        this.isVisible = true
        panel.layout = GridLayout(4, 2)
    }

    private fun addViews() {

        durationCbb.selectedItem = "2 semaines"

        lendButton.addActionListener {
            // check if searchUser yields codeback failure
            val (user, callbackCode) = controller.searchUser(User(inputFirstName.text, inputLastName.text, null, null))
            if (user.isBanned == 1) {
                JOptionPane.showMessageDialog(panel,
                        "${user.firstName} ${user.lastName} est sur liste rouge, veuillez régulariser sa situation " +
                                "avant de lui prêter des livres.",
                        "Succès",
                        JOptionPane.PLAIN_MESSAGE)
                this.dispose()
            } else if (user.isDelete == 1) {
                JOptionPane.showMessageDialog(panel,
                        "${user.firstName} ${user.lastName} a été supprimé d'Ashurbanipal",
                        "Succès",
                        JOptionPane.PLAIN_MESSAGE)
                this.dispose()
            } else {
                if (callbackCode == 0) {
                    val callbackCode2 = controller.lend(book, User(firstName.text, lastName.text, null, null, user.Id), durationCbb.selectedItem as String)
                    if (callbackCode2 == 0) {
                        JOptionPane.showMessageDialog(panel,
                                "<html><i>${book.title}</i> est prêté à ${inputFirstName.text} ${inputLastName.text}</html>",
                                "Succès",
                                JOptionPane.PLAIN_MESSAGE)
                        this.dispose()
                    } else {
                        JOptionPane.showMessageDialog(panel,
                                "Le prêt n'a pu être effectué.",
                                "Erreur",
                                JOptionPane.ERROR_MESSAGE)
                    }

                } else {
                    JOptionPane.showMessageDialog(panel,
                            "L'utilisateur n'a pu être identifié.",
                            "Erreur",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        }

        panel.add(firstName)
        panel.add(inputFirstName)
        panel.add(lastName)
        panel.add(inputLastName)
        panel.add(durationLabel)
        panel.add(durationCbb)
        panel.add(lendButton)
    }

}