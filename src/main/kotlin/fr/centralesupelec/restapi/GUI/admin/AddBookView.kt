package fr.centralesupelec.restapi.GUI.admin

import fr.centralesupelec.restapi.controller.Controller
import fr.centralesupelec.restapi.model.Genre
import java.awt.Dimension
import java.awt.GridLayout
import javax.swing.*


class AddBookView(private var controller: Controller): JFrame() {
    private val panel = JPanel()

    private val bookTitle = JLabel("Titre", SwingConstants.CENTER)
    private val inputBookTitle = JTextArea()
    private val author = JLabel("Auteur", SwingConstants.CENTER)
    private val inputAuthor= JTextArea()
    private val genre = JLabel("Genre", SwingConstants.CENTER)
    private val genreList = arrayOf(Genre.Policier, Genre.Poesie, Genre.Fantastique, Genre.Autres)
    private val genreComboBox= JComboBox(genreList)
    private val repeatLabel = JLabel("Nombre à ajouter", SwingConstants.CENTER)
    private val inputRepeat = JTextArea()
    private val addButton = JButton("Ajouter")


    init {
        setupMainFrame()
        addViews()
        this.contentPane.add(panel)
    }

    private fun setupMainFrame() {
        this.title = "Bibliothèque Ashurbanipal"
        defaultCloseOperation = DISPOSE_ON_CLOSE
        this.size = Dimension(400, 300)
        this.isVisible = true
        panel.layout = GridLayout(5, 2)



    }

    private fun addViews() {
        addButton.addActionListener{
            try {
                val callbackCode = this.controller.addBookC(inputBookTitle.text, inputAuthor.text,
                        genreComboBox.selectedItem as Genre, inputRepeat.text.toInt())
                when (callbackCode) {
                    0 -> {
                        JOptionPane.showMessageDialog(panel,
                                "<html><i>${inputBookTitle.text}</i> de ${inputAuthor.text} a été ajouté" +
                                        " ${inputRepeat.text} fois.</html>",
                                "Succès",
                                JOptionPane.PLAIN_MESSAGE)
                        inputBookTitle.text = ""
                        inputAuthor.text = ""
                        this.dispose()
                    }
                    1 -> {
                        JOptionPane.showMessageDialog(panel,
                                "Le livre n'a pu être ajouté.",
                                "Erreur",
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
            } catch (e: Exception) {
                JOptionPane.showMessageDialog(panel,
                        "Veuillez entrer un nombre en chiffres",
                        "Erreur",
                        JOptionPane.ERROR_MESSAGE)
            }
        }

        panel.add(bookTitle)
        panel.add(inputBookTitle)
        panel.add(author)
        panel.add(inputAuthor)
        panel.add(genre)
        panel.add(genreComboBox)
        panel.add(repeatLabel)
        panel.add(inputRepeat)
        panel.add(addButton)
    }

}