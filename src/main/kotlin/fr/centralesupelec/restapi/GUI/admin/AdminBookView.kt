package fr.centralesupelec.restapi.GUI.admin

import fr.centralesupelec.restapi.GUI.SearchBookView
import fr.centralesupelec.restapi.controller.Controller
import fr.centralesupelec.restapi.model.Book
import fr.centralesupelec.restapi.model.Genre
import fr.centralesupelec.restapi.model.Loan
import fr.centralesupelec.restapi.model.Role
import java.awt.BorderLayout
import java.awt.Dimension
import java.awt.GridLayout
import java.time.LocalDate
import javax.swing.*
import javax.swing.JOptionPane.OK_OPTION


class AdminBookView(private var controller: Controller, private val book: Book): JFrame() {
    private val panel = JPanel()

    private val bookTitleLabel = JLabel("Titre", SwingConstants.CENTER)
    private val bookTitleButton = JButton(book.title)
    private val authorLabel = JLabel("Auteur", SwingConstants.CENTER)
    private val authorButton = JButton(book.author)
    private val genre = JLabel("Genre", SwingConstants.CENTER)
    private val genreList = arrayOf(Genre.Policier, Genre.Poesie, Genre.Fantastique, Genre.Autres)
    private val genreComboBox = JComboBox(genreList)
    private val availabilityLabel = JLabel("", SwingConstants.CENTER)
    private val userButton = JButton()
    private val loanStateButton = JButton()
    private val prolongButton = JButton()
    private val historyButton = JButton("Historique des emprunts")
    private val deleteButton = JButton("Supprimer")

    init {
        setupMainFrame()
        addViews()
        this.contentPane.add(panel)
    }

    private fun setupMainFrame() {
        // Define frame, which is the main window on which every component relies
        this.title = "Bibliothèque Ashurbanipal"
        this.defaultCloseOperation = DISPOSE_ON_CLOSE
        this.size = Dimension(600, 400)
        this.isVisible = true
        panel.layout = GridLayout(6, 2)
    }


    private fun addViews() {

        if (book.isDelete == 1) {
            JOptionPane.showMessageDialog(panel,
                    "Le livre N°${book.Id} a été supprimé d'Ashurbanipal.",
                    "Erreur",
                    JOptionPane.ERROR_MESSAGE)
            println("Trying to view deleted book")
            this.dispose()
        }

        genreComboBox.selectedItem = book.genre
        panel.add(bookTitleLabel)
        panel.add(bookTitleButton)
        bookTitleButton.addActionListener {
            SearchBookView.search(panel, controller, book.title, book.author, Genre.Quelconque, Role.ADMIN)
        }
        panel.add(authorLabel)
        panel.add(authorButton)
        authorButton.addActionListener {
            SearchBookView.search(panel, controller, "", book.author, Genre.Quelconque, Role.ADMIN)
        }
        panel.add(genre)
        panel.add(genreComboBox)
        genreComboBox.addActionListener {
                val callbackCode = controller.editBookGenre(book, genreComboBox.selectedItem as Genre)
                if (callbackCode == 1) {
                    JOptionPane.showMessageDialog(panel,
                            "Le genre du livre n'a pu être changé avec succès.",
                            "Erreur",
                            JOptionPane.ERROR_MESSAGE);
                    genreComboBox.selectedItem = book.genre
            }
        }

        val (callbackCode, loan) = this.controller.getLoanStatus(book)
        when (callbackCode) {
            0 -> {
                if (loan?.dueDate != null && loan.returnedDate == null) {
                    availabilityLabel.text = "Emprunté du ${loan.madeDate} au ${loan.dueDate} par : "
                    userButton.text = loan.user.firstName + " " + loan.user.lastName

                    loanStateButton.text = "Marquer comme rendu"
                    loanStateButton.addActionListener {
                        controller.bookReturned(book.Id!!)
                        bookIsAvailable()
                    }

                    prolongButton.text = "Prolonger l'emprunt"
                    prolongButton.addActionListener {
                        showProlongOptions(loan)
                    }

                } else {
                    bookIsAvailable()
                }
            }
            1 -> {
                availabilityLabel.text = "Non trouvé dans la base de données"
                userButton.isVisible = false
                loanStateButton.isVisible = false
                prolongButton.isVisible = false
            }
        }

        panel.add(availabilityLabel)
        panel.add(userButton)
        panel.add(loanStateButton)
        panel.add(prolongButton)
        panel.add(historyButton)
        historyButton.addActionListener {
            LoanHistoryDialog(controller, book)
        }
        panel.add(deleteButton)
        deleteButton.addActionListener {
            val retour = JOptionPane.showConfirmDialog(this,
                    "Êtes-vous sûr.e de vouloir supprimer ce livre ?",
                    "titre",
                    JOptionPane.OK_CANCEL_OPTION)
            if (retour == OK_OPTION) {
                val callbackCodeDelete = controller.deleteBook(book)
                if (callbackCodeDelete == 1) {
                    JOptionPane.showMessageDialog(panel,
                            "Le livre n'a pu être supprimé.",
                            "Erreur",
                            JOptionPane.ERROR_MESSAGE);
                } else {
                    dispose()
                }
            }
        }

    }


    private fun bookIsAvailable(){
        availabilityLabel.text = "Disponible"
        userButton.isVisible = false
        loanStateButton.text = "Prêter"
        loanStateButton.addActionListener{
            LendBookView(controller, book)
            this.dispose()
        }
        prolongButton.isVisible = false
    }

    private fun showProlongOptions(loan: Loan){
        val possibilities = arrayOf<String>("1 semaine", "2 semaines", "3 semaines", "un mois")
        val s = JOptionPane.showInputDialog(panel, "Prolonger l'emprunt pour une durée de :",
                "Prolongation d'emprunt",
                JOptionPane.PLAIN_MESSAGE,
                null,
                possibilities,
                "1 semaine") as String

        val prolongedDate: LocalDate? = when (s) {
            "1 semaine" -> {
                loan.dueDate.plusWeeks(1)
            } "2 semaines" -> {
                loan.dueDate.plusWeeks(2)
            } "3 semaines" -> {
                loan.dueDate.plusWeeks(3)
            } "un mois" -> {
                loan.dueDate.plusWeeks(4)
            }
            else -> null
        }
        if (prolongedDate != null){
            controller.prolongLoan(book.Id!!, prolongedDate)
            availabilityLabel.text = "Emprunté du ${loan.madeDate} au $prolongedDate par : "
        } else {
            println("Choix de durée de prolongation: [$s] n'a pas été reconnu.")
            JOptionPane.showMessageDialog(panel,
                    "Si le problème persiste, contactez la bibliothèque.",
                    "Erreur",
                    JOptionPane.ERROR_MESSAGE)
        }
    }

    private class LoanHistoryDialog(controller: Controller, book: Book) : JDialog() {
        private val scrollPane = JScrollPane()

        init {
            setBounds(100, 100, 600, 300)
            defaultCloseOperation = DISPOSE_ON_CLOSE
            isVisible = true
            contentPane.layout = BorderLayout(0, 0)
            contentPane.add(scrollPane)

            val (callbackCode, loanList) = controller.getBookLoanHistory(book)
            var labelString = "<html><body><h3>   Historique des emprunts de <i>${book.title}</i> de ${book.author}</h3><br><br>"
            if (callbackCode == 0) {
                if (loanList != null) {
                    for (loan in loanList) {
                        labelString += "<p>-   ${loan.user.firstName} ${loan.user.lastName} : " +
                                "emprunt du ${loan.madeDate} au ${loan.dueDate}"
                        labelString += if (loan.returnedDate != null) {
                            if (loan.returnedDate > loan.dueDate) {
                                ", rendu le <span style=\"color:red;\">${loan.returnedDate}</span>"

                            } else {
                                ", rendu le ${loan.returnedDate}</p>"
                            }
                        } else {
                            ".</p>"
                        }
                    }
                }
            } else {
                labelString += "<p>Erreur de chargement de l'historique des emprunts.</p>"
            }
            labelString += "</body></html>"
            val lblContent = JLabel(labelString)
            scrollPane.setViewportView(lblContent)
        }
    }
}