package fr.centralesupelec.restapi.GUI.admin

import fr.centralesupelec.restapi.GUI.SearchBookView
import fr.centralesupelec.restapi.controller.Controller
import fr.centralesupelec.restapi.model.Role
import fr.centralesupelec.restapi.model.User
import java.awt.Dimension
import java.awt.GridLayout
import java.awt.event.KeyEvent
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import java.time.format.DateTimeFormatter
import javax.swing.*
import javax.swing.table.DefaultTableModel

open class AdminView(private var controller: Controller): JFrame() {

    private val panel = JPanel()

    init {
        setupMainFrame()
        addViews()
        this.contentPane.add(panel)
    }

    private fun setupMainFrame() {
        // Define frame, which is the main window on which every component relies
        this.title = "Ashurbanipal - Gestion administrative"
        this.defaultCloseOperation = EXIT_ON_CLOSE
        this.size = Dimension(1300, 300)
        this.isVisible = true
    }

    private fun addViews() {
        val tabbedPane = JTabbedPane()

        val panel1 = JPanel()
        tabbedPane.addTab("Livres", null, panel1, null)
        tabbedPane.setMnemonicAt(0, KeyEvent.VK_1)
        panel1.layout = GridLayout(2, 1)
        panel1.preferredSize = Dimension(300, 150)
        val searchBookButton = JButton("Rechercher un livre")
        searchBookButton.addActionListener{
            SearchBookView(controller, Role.ADMIN)
        }
        searchBookButton.preferredSize = Dimension(300,150)
        val addBookButton = JButton("Ajouter un livre")
        addBookButton.addActionListener{
            AddBookView(controller)
        }
        addBookButton.preferredSize = Dimension(300,150)
        panel1.add(searchBookButton)
        panel1.add(addBookButton)


        val panel2 = JPanel()
        tabbedPane.addTab("Utilisateurs", null, panel2, null)
        tabbedPane.setMnemonicAt(1, KeyEvent.VK_2)
        panel2.layout = GridLayout(2, 1)

        val searchUserButton = JButton("Rechercher un utilisateur")
        searchUserButton.addActionListener {
            SearchUserView(controller)
        }

        val viewusersButton = JButton("Voir tous les membres")

        viewusersButton.addActionListener {
            val (userList, callbackCode) = controller.searchAllUsers()
            if (callbackCode == 0) {
                if (userList != null) {
                    ViewUsersView(controller, userList)
                } else {
                    JOptionPane.showMessageDialog(panel,
                            "Se peut-il ? Ashurbanipal est vide ?")
                }

            } else {
                JOptionPane.showMessageDialog(panel,
                        "La recherche a conduit à une erreur.",
                        "Erreur",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
        panel2.add(searchUserButton)
        panel2.add(viewusersButton)

        val panel3 = JPanel()
        tabbedPane.addTab("Emprunts", null, panel3, null)
        tabbedPane.setMnemonicAt(2, KeyEvent.VK_3)
        val scrollPane: JScrollPane = generateLoanTable()
        scrollPane.preferredSize = Dimension(1000, 150)
        panel3.add(scrollPane)
        panel3.preferredSize = Dimension(1000,150)
        //Add the tabbed pane to this panel.
        panel.add(tabbedPane)

        //The following line enables to use scrolling tabs.
        tabbedPane.tabLayoutPolicy = JTabbedPane.SCROLL_TAB_LAYOUT
    }

    private fun generateLoanTable(): JScrollPane {
        val columnNames = arrayOf("Date de rendu", "Date du prêt", "Prénom", "Nom", "Titre", "Auteur")
        val data = loanListToArray()
        val table = JTable(data, columnNames)

        // On double click of table cell
        table.addMouseListener(object : MouseAdapter() {
            override fun mouseClicked(me: MouseEvent) {
                if (me.clickCount == 2) {     // to detect double click events

                    val target = me.source as JTable
                    val row = target.selectedRow // select a row

                    val firstName = (table.model.getValueAt(row, 2) as String)
                    val lastName = (table.model.getValueAt(row, 3) as String)
                    val userQuery = User(firstName, lastName, null, Role.UNKNOWN)

                    val (user, callbackCode) = controller.searchUser(userQuery)

                    if (controller.isDeletedUser(User(firstName, lastName, null, null))) {
                        JOptionPane.showMessageDialog(panel,
                                "L'utilisateur a été supprimé",
                                "Erreur",
                                JOptionPane.ERROR_MESSAGE);
                    } else {
                        if (callbackCode == 0) {
                            SearchUserResultsView(controller, user)
                        } else {
                            JOptionPane.showMessageDialog(panel,
                                    "La recherche a conduit à une erreur.",
                                    "Erreur",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }
            }
        })

        //following block makes cells not editable
        val tableModel: DefaultTableModel = object : DefaultTableModel(data, columnNames) {
            override fun isCellEditable(row: Int, column: Int): Boolean {
                return false
            }
        }
        table.model = tableModel

        val scrollPane = JScrollPane(table)
        return scrollPane
    }

    private fun loanListToArray(): Array<Array<String>> {
        val formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd")

        val mutableLoanList: MutableList<Array<String>> = mutableListOf()
        val (_, loanList) = controller.getCurrentLoans()

        if (loanList != null) {
            for (loan in loanList) {
                val dueDateString: String = loan.dueDate.format(formatter)
                val madeDateString: String = loan.madeDate.format(formatter)
                mutableLoanList.add(arrayOf(dueDateString, madeDateString,
                        loan.user.firstName, loan.user.lastName,
                        loan.book.title, loan.book.author))
            }
        }
        return mutableLoanList.toTypedArray()
    }




}