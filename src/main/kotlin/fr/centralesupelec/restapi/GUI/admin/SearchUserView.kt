package fr.centralesupelec.restapi.GUI.admin

import fr.centralesupelec.restapi.GUI.admin.SearchUserResultsView
import fr.centralesupelec.restapi.controller.Controller
import fr.centralesupelec.restapi.model.Role
import fr.centralesupelec.restapi.model.User
import java.awt.Dimension
import java.awt.GridLayout
import javax.swing.*


class SearchUserView(private var controller: Controller): JFrame() {
    private val panel = JPanel()
    private val firstNameLabel = JLabel("Prénom", SwingConstants.CENTER)
    private val inputFirstName = JTextArea()
    private val lastNameLabel = JLabel("Nom", SwingConstants.CENTER)
    private val inputLastName = JTextArea()
    private val searchButton = JButton("Rechercher")

    init {
        setupMainFrame()
        addViews()
        this.contentPane.add(panel)
    }

    private fun setupMainFrame() {
        // Define frame, which is the main window on which every component relies
        this.title = "Bibliothèque Ashurbanipal"
        this.defaultCloseOperation = JFrame.DISPOSE_ON_CLOSE
        this.size = Dimension(400, 300)
        this.isVisible = true
        panel.layout = GridLayout(3, 2)
    }

    private fun addViews() {

        searchButton.addActionListener{
            val userQuery = User(inputFirstName.text, inputLastName.text, null, Role.UNKNOWN)
            val (user, callbackCode) = controller.searchUser(userQuery)
            if (callbackCode == 0) {
                SearchUserResultsView(controller, user)
                this.dispose()
            } else {
                JOptionPane.showMessageDialog(panel,
                        "La recherche a conduit à une erreur.",
                        "Erreur",
                        JOptionPane.ERROR_MESSAGE);
            }
        }


        panel.add(firstNameLabel)
        panel.add(inputFirstName)
        panel.add(lastNameLabel)
        panel.add(inputLastName)
        panel.add(searchButton)
    }


}