package fr.centralesupelec.restapi.GUI.admin

import fr.centralesupelec.restapi.controller.Controller
import fr.centralesupelec.restapi.model.Role
import fr.centralesupelec.restapi.model.User
import java.awt.Dimension
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import javax.swing.*
import javax.swing.table.DefaultTableModel

class ViewUsersView (val controller: Controller, val userList: MutableList<User>): JFrame() {
    private val panel = JPanel()

    init {
        setupMainFrame()
        addViews()
        this.contentPane.add(panel)
    }

    private fun setupMainFrame() {
        // Define frame, which is the main window on which every component relies
        this.title = "Bibliothèque Ashurbanipal"
        this.defaultCloseOperation = JFrame.DISPOSE_ON_CLOSE
        this.size = Dimension(600, 300)
        this.isVisible = true
    }

    private fun addViews() {

        val columnNames = arrayOf("Prénom", "Nom", "Role", "Listé rouge")
        val data = userListToArray()
        val table = JTable(data, columnNames)

        // On double click of table cell
        table.addMouseListener(object : MouseAdapter() {
            override fun mouseClicked(me: MouseEvent) {
                if (me.clickCount == 2) {     // to detect double click events

                    val target = me.source as JTable
                    val row = target.selectedRow // select a row

                    val firstName = (table.model.getValueAt(row, 0) as String)
                    val lastName = (table.model.getValueAt(row, 1) as String)
                    val userQuery = User(firstName, lastName, null, Role.UNKNOWN)

                    val (user, callbackCode) = controller.searchUser(userQuery)

                    if (controller.isDeletedUser(User(firstName, lastName, null, null))) {
                        JOptionPane.showMessageDialog(panel,
                                "L'utilisateur a été supprimé",
                                "Erreur",
                                JOptionPane.ERROR_MESSAGE);
                    } else {
                        if (callbackCode == 0) {
                            SearchUserResultsView(controller, user)
                        } else {
                            JOptionPane.showMessageDialog(panel,
                                    "La recherche a conduit à une erreur.",
                                    "Erreur",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }
            }
        })

        //following block makes cells not editable
        val tableModel: DefaultTableModel = object : DefaultTableModel(data, columnNames) {
            override fun isCellEditable(row: Int, column: Int): Boolean {
                return false
            }
        }
        table.model = tableModel

        val scrollPane = JScrollPane(table)
        panel.add(scrollPane)
    }



    private fun userListToArray(): Array<Array<String>> {
        val mutableUserList: MutableList<Array<String>> = mutableListOf()
        for (user in userList) {
            if (user.isBanned == 0) {
                mutableUserList.add(arrayOf(user.firstName, user.lastName, user.role!!.name, ""))
            } else {
                println("${user.firstName} is banned")
                mutableUserList.add(arrayOf(user.firstName, user.lastName, user.role!!.name, "Oui"))
            }
        }
        return mutableUserList.toTypedArray()
    }

}