package fr.centralesupelec.restapi.GUI

import fr.centralesupelec.restapi.GUI.admin.AdminView
import fr.centralesupelec.restapi.GUI.member.MemberView
import fr.centralesupelec.restapi.controller.Controller
import fr.centralesupelec.restapi.model.Role
import fr.centralesupelec.restapi.model.User
import java.awt.Dimension
import java.awt.GridLayout
import javax.swing.*


class Homepage(private var controller: Controller): JFrame() {
    private val panel = JPanel()

    private val firstName = JLabel("Prénom", SwingConstants.CENTER)
    private val inputFirstName = JTextArea()
    private val lastName = JLabel("Nom", SwingConstants.CENTER)
    private val inputLastName = JTextArea()
    private val pass = JLabel("Mot de passe", SwingConstants.CENTER)
    private val inputPass = JPasswordField()
    private val loginButton = JButton("Connexion")
    private val registerButton = JButton("Créer un compte")



    init {
        setupMainFrame()
        addViews()
        this.contentPane.add(panel)
    }

    private fun setupMainFrame() {
        // Define frame, which is the main window on which every component relies
        this.title = "Bibliothèque Ashurbanipal"
        this.defaultCloseOperation = EXIT_ON_CLOSE
        this.size = Dimension(300, 150)
        this.isVisible = true
        panel.layout = GridLayout(4, 2)
    }

    private fun addViews() {
        loginButton.addActionListener {
            val (callbackCode, role) = this.controller.loginUserC(inputFirstName.text, inputLastName.text, inputPass.text)
            when (callbackCode) {
                0 ->{
                    if (role == Role.MEMBER) {
                        val memberView = MemberView(controller, getCurrentUser())
                    } else {
                        val adminView = AdminView(controller)
                    }
                    this.dispose()

                }
                1 -> {
                    //custom title, error icon
                    JOptionPane.showMessageDialog(panel,
                            "Si le problème persiste, veuillez contacter la bibliothèque.",
                            "Erreur",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        }

        registerButton.addActionListener {
            val callbackCode = this.controller.registerUserC(inputFirstName.text, inputLastName.text, inputPass.text)
            when (callbackCode) {
                0 -> {
                    JOptionPane.showMessageDialog(panel,
                            "Merci d'avoir créé un compte à Ashurbanipal ! :)",
                            "Succès",
                            JOptionPane.PLAIN_MESSAGE);

                    // Now, log in (as member).
                    val memberView = MemberView(controller, getCurrentUser())
                    this.dispose()
                                    }
                1 -> {
                    JOptionPane.showMessageDialog(panel,
                            "Si le problème persiste, contactez la bibliothèque.",
                            "Erreur",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        }

        panel.add(firstName)
        panel.add(inputFirstName)
        panel.add(lastName)
        panel.add(inputLastName)
        panel.add(pass)
        panel.add(inputPass)
        panel.add(loginButton)
        panel.add(registerButton)
    }

    private fun getCurrentUser(): User {
        val (user, _) = controller.searchUser(User(inputFirstName.text, inputLastName.text, null, Role.UNKNOWN))
        return user
    }

}