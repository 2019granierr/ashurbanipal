package fr.centralesupelec.restapi.GUI

import fr.centralesupelec.restapi.GUI.admin.SearchBookResultsView
import fr.centralesupelec.restapi.controller.Controller
import fr.centralesupelec.restapi.model.Genre
import fr.centralesupelec.restapi.model.Role
import java.awt.Dimension
import java.awt.GridLayout
import javax.swing.*

class SearchBookView(private var controller: Controller, private val userrole: Role): JFrame() {
    private val panel = JPanel()

    private val bookTitleQuery = JLabel("Titre", SwingConstants.CENTER)
    private val inputBookTitleQuery = JTextArea()
    private val authorQuery = JLabel("Auteur", SwingConstants.CENTER)
    private val inputAuthorQuery = JTextArea()
    private val genreQuery = JLabel("Genre", SwingConstants.CENTER)
    private val genreList = arrayOf(Genre.Quelconque, Genre.Policier, Genre.Poesie, Genre.Fantastique, Genre.Autres)
    private val genreComboBox= JComboBox(genreList)
    private val searchButton = JButton("Rechercher les livres")


    init {
        setupMainFrame()
        addViews()
        this.contentPane.add(panel)
    }

    private fun setupMainFrame() {
        // Define frame, which is the main window on which every component relies
        this.title = "Bibliothèque Ashurbanipal"
        this.defaultCloseOperation = JFrame.DISPOSE_ON_CLOSE
        this.size = Dimension(400, 300)
        this.isVisible = true
        panel.layout = GridLayout(4, 2)
    }

    private fun addViews() {

        searchButton.addActionListener {
            search(panel, controller, inputBookTitleQuery.text, inputAuthorQuery.text,
                    genreComboBox.selectedItem as Genre, userrole)
            this.dispose()
        }


        panel.add(bookTitleQuery)
        panel.add(inputBookTitleQuery)
        panel.add(authorQuery)
        panel.add(inputAuthorQuery)
        panel.add(genreQuery)
        panel.add(genreComboBox)
        panel.add(searchButton)
    }

    companion object {
        fun search(panel: JPanel, controller: Controller, title: String, author: String, genre: Genre, userrole: Role) {
            val (callbackCode, resultList) = controller.searchBookC(title, author, genre)
            when (callbackCode) {
                0 -> {
                    if (resultList != null) {
                        println("Results found have size: " + resultList.size)
                        SearchBookResultsView(controller, resultList, title, author, genre, userrole)
                    }
                }
                1 -> {
                    JOptionPane.showMessageDialog(panel,
                            "La recherche a conduit à une erreur.",
                            "Erreur",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }

}
