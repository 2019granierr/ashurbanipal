package fr.centralesupelec.restapi.GUI.member

import fr.centralesupelec.restapi.GUI.SearchBookView
import fr.centralesupelec.restapi.controller.Controller
import fr.centralesupelec.restapi.model.Book
import fr.centralesupelec.restapi.model.Genre
import fr.centralesupelec.restapi.model.Role
import java.awt.Dimension
import java.awt.GridLayout
import javax.swing.*

class MemberBookView(private var controller: Controller, private val book: Book): JFrame() {

    private val panel = JPanel()

    private val bookTitleLabel = JLabel("Titre", SwingConstants.CENTER)
    private val bookTitleButton = JButton(book.title)
    private val authorLabel = JLabel("Auteur", SwingConstants.CENTER)
    private val authorButton = JButton(book.author)
    private val genreLabel = JLabel("Genre : ${book.genre}", SwingConstants.CENTER)
    private val availabilityLabel = JLabel("", SwingConstants.CENTER)

    init {
        setupMainFrame()
        addViews()
        this.contentPane.add(panel)
    }

    private fun setupMainFrame() {
        this.title = "Bibliothèque Ashurbanipal"
        this.defaultCloseOperation = DISPOSE_ON_CLOSE
        this.size = Dimension(300, 200)
        this.isVisible = true
        panel.layout = GridLayout(3, 2)
    }


    private fun addViews() {

        if (book.isDelete == 1) {
            JOptionPane.showMessageDialog(panel,
                    "Le livre N°${book.Id} a été supprimé d'Ashurbanipal.",
                    "Erreur",
                    JOptionPane.ERROR_MESSAGE)
            println("Trying to view deleted book")
            this.dispose()
        }

        bookTitleButton.addActionListener {
            SearchBookView.search(panel, controller, book.title, book.author, Genre.Quelconque, Role.ADMIN)
        }

        authorButton.addActionListener {
            SearchBookView.search(panel, controller, "", book.author, Genre.Quelconque, Role.ADMIN)
        }

        val (callbackCode, loan) = this.controller.getLoanStatus(book)
        when (callbackCode) {
            0 -> {
                if (loan?.dueDate != null && loan.returnedDate == null) {
                    availabilityLabel.text = "Emprunté jusqu'au ${loan.dueDate}"
                } else {
                    availabilityLabel.text = "Disponible en ce moment"
                }
            }
            1 -> {
                availabilityLabel.text = "Non trouvé dans la base de données"
            }
        }


        panel.add(bookTitleLabel)
        panel.add(bookTitleButton)
        panel.add(authorLabel)
        panel.add(authorButton)
        panel.add(genreLabel)
        panel.add(availabilityLabel)
    }

}