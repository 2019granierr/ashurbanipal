package fr.centralesupelec.restapi.GUI.member

import fr.centralesupelec.restapi.GUI.SearchBookView
import fr.centralesupelec.restapi.GUI.admin.LoanHistoryDialog
import fr.centralesupelec.restapi.GUI.admin.getCurrentLoansString
import fr.centralesupelec.restapi.controller.Controller
import fr.centralesupelec.restapi.model.Role
import fr.centralesupelec.restapi.model.User
import java.awt.Dimension
import java.awt.FlowLayout
import java.awt.GridLayout
import java.awt.event.KeyEvent
import javax.swing.*


open class MemberView(val controller: Controller, private val currentUser: User): JFrame() {
    val panel = JPanel()

    init {
        setupMainFrame()
        addViews()
        this.contentPane.add(panel)
    }

    fun setupMainFrame() {
        // Define frame, which is the main window on which every component relies
        this.title = "Library Interface"
        this.defaultCloseOperation = EXIT_ON_CLOSE
        this.size = Dimension(750, 500)
        this.isVisible = true
    }

    fun addViews() {
        val tabbedPane = JTabbedPane()
        tabbedPane.size = Dimension(600, 400)

        val panel1 = JPanel()
        tabbedPane.addTab("Rechercher", null, panel1, null)
        tabbedPane.setMnemonicAt(0, KeyEvent.VK_1)

        val searchButton = JButton("Effectuer une recherche")
        searchButton.addActionListener {
            SearchBookView(controller, Role.MEMBER)
        }
        panel1.add(searchButton)
        panel1.preferredSize = Dimension(600, 400)

        val panel2 = JPanel()
        tabbedPane.addTab("Mon compte", null, panel2, null)
        tabbedPane.setMnemonicAt(1, KeyEvent.VK_2)

        val isBannedLabel = JLabel("")
        if (currentUser.isBanned == 0) {
            isBannedLabel.text = "<html><body><h3>${currentUser.firstName} ${currentUser.lastName}, " +
                    "honorable membre d'Ashurbanipal</h3></body></html>"
        } else {
            isBannedLabel.text = "<html><body>${currentUser.firstName} ${currentUser.lastName}, " +
                    "vous avez fait preuve de laxisme dans vos emprunts.<br>" +
                    "Veuillez <b>régulariser votre situation</b> auprès de la secrétaire désagréable de l'accueil.</body></html>"
        }

        var currentLoansString = "<html><body><h4>   Emprunts en cours</h4>" +
                "<br><br>${getCurrentLoansString(controller, currentUser)}</html></body>"
        val currentLoansLabel = JLabel(currentLoansString)

        val loanHistoryButton = JButton("Historique")
        loanHistoryButton.addActionListener {
            LoanHistoryDialog(controller, currentUser)
        }

        panel2.layout = GridLayout(3,1)
        panel2.add(isBannedLabel)
        panel2.add(currentLoansLabel)
        panel2.add(loanHistoryButton)
        panel.add(tabbedPane)

        //The following line enables to use scrolling tabs.
        tabbedPane.tabLayoutPolicy = JTabbedPane.SCROLL_TAB_LAYOUT
    }
}