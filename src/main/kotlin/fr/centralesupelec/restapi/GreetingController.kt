package fr.centralesupelec.restapi

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.concurrent.atomic.AtomicLong


@RestController
class GreetingController {
    private val counter = AtomicLong()
    @Autowired
    var jdbcTemplate =  JdbcTemplate()

    @GetMapping("/greeting")
    fun greeting(@RequestParam(value = "name", defaultValue = "World") name: String?): String {
        return String.format(template, name)

        //return Greeting(counter.incrementAndGet(), String.format(template, name))
    }
    @GetMapping("/readMyEntry")
    fun readMyEntry(@RequestParam(value = "name", defaultValue = "World") name: String?): MyEntry? {
        val rowMapper = RowMapper<MyEntry>{rs,
                                           roCount -> MyEntry(rs.getInt("id"),
                rs.getString("name"))}
        var o = jdbcTemplate.queryForObject("SELECT * FROM TEST WHERE id=1;", rowMapper)
        return o
    }


    companion object {
        private const val template = "Hello, %s!"
    }
}