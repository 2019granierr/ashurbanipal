package fr.centralesupelec.restapi.model

class Book(val title: String, val author: String, val genre: Genre, val Id: Int? = null, val isDelete: Int = 0)


enum class Genre {
    Policier, Fantastique, Poesie, Autres, Quelconque;
}