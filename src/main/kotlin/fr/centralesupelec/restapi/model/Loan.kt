package fr.centralesupelec.restapi.model

import java.time.LocalDate

class Loan(val book: Book,
           val user: User,
           val madeDate: LocalDate, // date when loan was made
           var dueDate: LocalDate,
           val returnedDate: LocalDate?)