package fr.centralesupelec.restapi.model

class User(val firstName: String,
           val lastName: String,
           var password: String?,
           var role: Role?,
           var Id: Int? = null,
           val isDelete: Int = 0,
           val isBanned: Int = 0) {
    // user could change password (hence var)
    // password safety??? :(
    // user might be promoted to admin (hence var)
}

enum class Role {
    ADMIN, MEMBER, UNKNOWN;
}